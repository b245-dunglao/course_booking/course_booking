const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// Controllers

// This controller will create or register a user on our db/database

module.exports.userRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already registered!")
			}
			else {
				let newUser = new User({
					firstName : input.firstName,
					lastName : input.lastName,
					email: input.email,
					password : bcrypt.hashSync(input.password, 10),
					mobileNo : input.mobileNo
				})

				newUser.save()
					.then(save => {
						return response.send("You are now registered to our website!")
					})
			}
		})
		.catch(error => {
			return response.send(error)
		})
} 



// User Authentication
module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	// Possible scenarios in logging in 
		// 1. Email is not yet registered.
		// 2. Email is registered but the password is wrong. 
		User.findOne({email: input.email})
		.then(result => {
			if (result === null) {
				return response.send("Email is not registred. Register first before logging in!")
			}else {
				// we have to verify if the password is correct.
				// the "compareSync" method is used for compare a non encrypted password to the encrypted password.
				// it returns bollean value, if match true value will return. 
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);

					// no need for === true as isPasswordCorrect is truthy
				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)})
				}else{
					return response.send("Password is incorrect!")
				}
			}
		})
		.catch(error => {
			return response.send(error)
		})

} 


// User details retrieval -activity

module.exports.userAccount = (request, response) =>{
	let input = request.body;

	User.findOne({_id: input._id})
	.then(result =>{
		if ( result === null ) {
			return response.send("Email is not registered.");
		}else{
			result.password = "Confidential"
			return response.send(result);
		}

	})

}

//Activity Solution

module.exports.getProfile = (request, response) =>{
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData._id)
	.then(result =>{
		// avoid to expose sensitive information such as password. 
		result.password = "Confidential";

		return response.send(result);

	})

}

// Controller for user enrollment
	// 1. We can get the id of the user by decoding the jwt
	// 2. We can get the courseId by using the request params

// module.exports.enrollCourse = async (request, response) => {
// 	// extract information from the payload. 
// 	const userData = auth.decode(request.headers.authorization);
// 	// get the courseId by targetting the params in the url.
// 	const courseId = request.params.courseId;


	/*2 things that we need to do in this controller
		First, to push the course Id in the enropllements property of the user.
		Second, to push the userId in the enrollees property of the course. 

	*/

	/*let isUserUpdated = await User.findById(userData._id)
	.then(result => {
		if(result === null){
			return false
		}
		else {
			result.enrollments.push({courseID: courseId})

			return result.save()
			.then(save => true)
			.catch(error=>false)
		}
	})

	let isCourseUpdated = await Course.findById(courseId)
	.then( result => {
		if(result === null){
			return false;
		}
		else {
			result.enrolles.push({userId: userData._id})
			return result.save()
			.then(save => true)
			.catch(error=>false)
		}
	})
	console.log( isUserUpdated + isCourseUpdated)
	if (isCourseUpdated && isUserUpdated){
		return response.send("The course is now enrolled!")
	}
	else{
		return response.send("There was an error during the enrollement. Please try again")
	}

}
*/

// Refactor - Activity s41 - improve limitation to users only
module.exports.enrollCourse = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;

	if (!mongoose.Types.ObjectId.isValid(courseId)) {
		return response.send("Please check course ID.")
	}
	else if (userData.isAdmin){
		return response.send("Please contact your administrator.")
	}
	else{
		await Course.findById(courseId)
		.then( result => {
			if(result === null){
				return response.send("Please check course ID.")
			}
			else {
				result.enrolles.push({userId: userData._id})
				return result.save()
				.then(save => {
					return response.send("The course is now enrolled!")
				})
				.catch(error => {
							return response.send(error)
						})

				User.findById(userData._id)
							.then(result => {
								if(result !== null){
									result.enrollments.push({courseID: courseId})
									return result.save()
								}
								else {
									return response.send("Please contact your admin.")			
								}
							})
			}
		})		
	}
}