const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");
const User = require("../Models/usersSchema.js");
const mongoose = require("mongoose");


// Create a nwe course

/*
	Steps: 
		1. Create a new Course object using the mongoose model and the information forom the request body and the id from the header. 
		2. save the user to the database. 

*/

// add course 
//refactor add course to confirm admin rights - activity

module.exports.addCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

		if (userData.isAdmin === true){

			let input = request.body;

				let newCourse = new Course({
					name: input.name,
					description: input.description,
					price: input.price 
				});
				// Saves the created object to our database
				return newCourse.save()
				.then(course => {
					console.log(course);
					response.send(course);
				})
				// course creation failed
				.catch(error => {
						return response.send(error)
					})
			
		}else{
			return response.send("Please contact your administrator")
		}
	
}


// Create a controller wherein it will retrieved all the courses (active/inactive courses)

module.exports.allCourses = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You do not have access to this route!");
	}
	else {
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}

}


// Create a controller wherein it will retrieve course that are active. 

module.exports.allActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))

}



/*
Mini Activity

1. You are going to create a route wherein it can retrieve all inactive courses.
2. Make sure that the admin users only are the one who can access this route.
*/

// inactive courses
module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You do not have access to this route!");
	}
	else {
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
}

//  This controller will get the details of 
module.exports.courseDetails = (request, response) => {
	// to get the params from the url
	const courseId = request.params.id;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

// This controller is for updating specific course 
// Business logic
// 	1. We are going to edit/update the course that is stored in the params. 
module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	const courseId = request.params.courseId;

	if(!userData.isAdmin){
		return response.send("You do not have access to this route!");
	}
	else {
		Course.findOne({_id: courseId})
		.then(result => {
			if (result === null){
				return response.send("CourseId is invalid.")
			}
			else {
				let updatedCourse = { 
							name : input.name,
							description: input.description,
							price: input.price
						}

						Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
						.then(result => response.send(result))
						.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
}


// Archive course - activity
module.exports.archiveCourse = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	const courseId = request.params.courseId;

	if(!userData.isAdmin){
		return response.send("You do not have access to this route!");
	}
	else {
		if (!mongoose.Types.ObjectId.isValid(courseId)){
			return response.send("CourseId is invalid.")
		}
		else{
			Course.findOne({_id: courseId})
			.then(result => {
				if (result !== null){
					Course.findByIdAndUpdate(courseId, {isActive:input.isActive}, {new: true})
					.then(result => response.send(result.isActive))
					.catch(error => response.send(error))

				}
				else {
					
					return response.send("CourseId is invalid.")
				}
			})
			.catch(error => response.send(error))
		}
	}
}