const express = require("express");
const router = express.Router();
const auth = require("../auth.js");


const courseController = require("../Controllers/courseController.js");

// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

// Route for retrieving all active courses.
router.get("/allActive", courseController.allActiveCourses);

router.get("/allInactive", auth.verify,  courseController.allInactiveCourses);

// Routes for retrieving detail/s of specific course
router.get("/courseId/:id", courseController.courseDetails)

router.put("/update/:courseId", auth.verify, courseController.updateCourse);

router.patch("/:courseId/archive", auth.verify, courseController.archiveCourse);

module.exports = router; 	