const express = require("express");
const router = express.Router();
const auth = require("../auth.js");


const userController = require("../Controllers/userController.js")

// [Routes]

// This is responsible for thje registration of the user

router.post("/register", userController.userRegistration);

// This route is for the user authentication
router.post("/login", userController.userAuthentication);

// This is router to retrieve the details of the user - activity
// router.post("/details", auth.verify, userController.getProfile);
// - was updated because no data from the user is needed to get the information. 
router.get("/details", auth.verify, userController.getProfile);

router.post("/enrollment/:courseId", auth.verify, userController.enrollCourse);

module.exports = router; 