const express = require("express");
const mongoose = require("mongoose");
// by default, our backend's CORS setting will prevernt any application outside our Express JS app to process the request. Using the CORS package, it will allow us to manipulate this and control what applications may use our app. 

// Allows our backend application to be available to our frontend application. 
// Allows us to control the app's Cross Origin Resource Sharing.
// installed package via terminal with code : npm install cors 
const cors = require("cors");

const userRoutes = require("./Routes/userRoutes.js");
const courseRoutes = require("./Routes/courseRoutes.js")


const port = 3001;
const app = express();

mongoose.set('strictQuery', true);


// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@b245-dunglao.tbtmdrd.mongodb.net/batch245_Course_API_Dunglao?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Check connection
let db = mongoose.connection;

// Error catcher - Error handling
db.on("error", console.error.bind(console, "Connection Error!"));

// Confirmation of the connection - Validation of the connection
db.once("open",()=> console.log("We are now connected to the cloud!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// Routing
app.use("/user", userRoutes);
app.use("/course", courseRoutes);



app.listen(port, () => console.log(`Server is running at port ${port}`));