const mongoose = require("mongoose");


const usersSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "firstName of user is required!"]
	},
	lastName : {
		type: String,
		required: [true, "lastName of user is required!"]
	},
	email : {
		type: String,
		required: [true, "email of user is required!"]
	},
	password : {
		type: String,
		required: [true, "password of user is required!"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	mobileNo : {
		type: String,
		required: [true, "mobileNo of user is required!"]
	},
	enrollments : [
		{
			courseID :{
				type: String,
				required: [true, "courseID of the user is required!"]
			},
			enrolledOn :{
				type: Date,
				default: new Date()
			},
			status :{
				type: String,
				default: "Enrolled"
			}
		}

	]



})


module.exports = mongoose.model("User", usersSchema);